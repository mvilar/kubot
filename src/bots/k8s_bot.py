# -*- coding: utf-8 -*-
from kubernetes import client, config
from bots.bot_control import bot_control


class k8s_bot(bot_control):
    actionsSpec =   {
                "list": {"minArgs": 1 , 
                        "helpShort" : "*Listar* pods ou deploys",
                        "help" : "Posso exibir listagem de *aplicação*, *pods* e de *nodes*"},
                }
    
    
    def __init__(self, slackClient, namespaces=[],useLocalKubeconfig=False):
        bot_control.__init__(self,slackClient)
        if useLocalKubeconfig:
            config.load_kube_config()
        else:
            config.load_incluster_config()
        self.kubeCore=client.CoreV1Api()


    def list(self,slackMessage):
        params = self.prepareParams(slackMessage.get("text"))
        print params
        if "node" in params[0]:
            self.listNodes(slackMessage)
        else:
            reply="Não achei essa opção."
            self.sc.replyMessage(reply,slackMessage.get("channel"))


    def listNodes(self,slackMessage):
        nodeList=[]
        tableHeader=["Nome","Tipo","Status"]
        colSize=[33,7,7,30]
        msg="Segue a lista:\n```\n"
        for node in self.kubeCore.list_node().items:
            nodeHealth=self.checkNodeHealth(node)
            if len(nodeHealth.get("status")) > colSize[2]: colSize[2] = len(nodeHealth.get("status"))
            nodeList.append(
                    [
                    node.metadata.name,
                    node.metadata.labels.get("kubernetes.io/role"),
                    nodeHealth.get("status")
                    ]
                )
        msg+=self.createTextTable(nodeList,colSize,tableHeader,"| ")
        msg+="```"
        self.sc.replyMessage(msg,slackMessage.get("channel"))


    def checkNodeHealth(self,nodeInfo):
        readyStatus="None"
        tags={"True": "Ready", "False": "Problem", "Unknown": "Unknown", "None":"None"}
        problems=[]
        goodStatus={"OutOfDisk": "False", 
                    "Ready": "True", 
                    "MemoryPressure": "False", 
                    "DiskPressure": "False",
                    "PIDPressure": "False",
                    "NetworkUnavailable": "False",
                    "ConfigOK": "True"
                    }
        for condition in nodeInfo.status.conditions:
            if condition.type in goodStatus:
                if condition.status != goodStatus.get(condition.type):
                    problems.append("%s: %s" % (condition.reason,condition.message))
            if condition.type == "Ready":
                readyStatus=tags.get(condition.status)
            print condition
        if len(problems) > 0:
            readyStatus+=",Problem"
        if nodeInfo.spec.unschedulable:
            readyStatus+=",NoSchedulable"
        nodeStatus={
                "status": readyStatus,
                "problemsList": problems,
                "problemsCount": len(problems)
            }
        return nodeStatus

