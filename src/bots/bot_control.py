# -*- coding: utf-8 -*-

class bot_control:
    commandDict = {
            "list": ["list","lista","listar","listagem"],
            "get":  ["get","pegar","pegue"],
            "help": ["help","ajuda","ajude-me"]
        }
    wordsClean = ["o", "a", "os", "as", "de", "da", "do", "com", "sobre"]
    actionsSpec = {}


    def __init__(self, slackClient):
        self.sc = slackClient


    def checkCommand(self,text):
        for command in self.commandDict:
            if text.lower() in self.commandDict.get(command):
                return command
        return None


    def isForMe(self,event, bot_id, channels=[]):
        try:
            if channels and event.get('channel') not in channels:
                return False
            if str(bot_id) in event.get('text').strip().split()[0]:
                return True
        except Exception as e:
            print e
            return None


    def generalHelp(self,slackMessage,unknowCommand=False):
        reply = "<@%s> " % slackMessage.get("user")
        if unknowCommand == True:
            reply += "Desculpe, não entendi sua solicitação.\n"
        reply += "Posso te ajudar com os seguintes comandos:"
        for action in self.actionsSpec:
            reply += "\n - %s" % self.actionsSpec[action].get("helpShort")
        reply += "\nSe quiser mais detalhes informe o comando. Exemplo: ajuda sobre listar"
        self.sc.replyMessage(reply,slackMessage.get("channel"))


    def prepareParams(self,msg,cleanSmallWords=True):
        # Remove first 2 params
        msg=msg.strip().split()[2:]
        if cleanSmallWords:
            msgTemp=msg
            for word in msgTemp:
                if word in self.wordsClean:
                    msg.remove(word)
        return msg
    
    def createTextTable(self,list,columnsSize=[15,15,15,15,15,15],header=None,div=" "):
        textList=""
        if header:
            list.insert(0,header)
        for line in list:
            colCount=0
            if div != " ":
                textList+=div
            for column in line:
                textList+="%s%s" % (
                        column[:columnsSize[colCount]].ljust(columnsSize[colCount]),
                        div
                    )
                colCount+=1
            textList+="\n"
        return textList


    def help(self,slackMessage):
        params=self.prepareParams(slackMessage.get("text"))
        if len(params) > 0:
            command = self.checkCommand(params[0])
            if command and self.actionsSpec.get(command):
                reply="%s" % self.actionsSpec.get(command).get("help")
                self.sc.replyMessage(reply,slackMessage.get("channel"))
            else:
                self.generalHelp(slackMessage,True)
        else:
            self.generalHelp(slackMessage)

    def sendMessage(self):
        print "oi"