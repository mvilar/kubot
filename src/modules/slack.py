# -*- coding: utf-8 -*-

from slackclient import SlackClient
import json

class slack(SlackClient):

    def __init__(self,token):
        SlackClient.__init__(self,token)
        botInfo=self.api_call("auth.test")
        self.botId = botInfo.get("user_id")
        self.botName = botInfo.get("user")


    def replyMessage(self,message,channel):
        if isinstance(message,dict):
            self.api_call(
                "chat.postMessage",
                channel=channel,
                attachments=json.dumps(message),
                username=self.botName,
                as_user="true"
            )
        else:
            self.api_call(
                "chat.postMessage",
                channel=channel,
                text=message,
                username=self.botName,
                as_user="true"
            )

        