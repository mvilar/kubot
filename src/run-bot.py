#!/usr/bin/python
# -*- coding: utf-8 -*-
from modules.slack import slack
from bots.k8s_bot import k8s_bot
import time
import sys
import os

'''
bot methods: https://api.slack.com/bot-users#methods
https://slackapi.github.io/python-slackclient/real_time_messaging.html#rtm-start-vs-rtm-connect
https://github.com/slackapi/python-slackclient

bot info sc.api_call("bots.info")
'''

reload(sys)
sys.setdefaultencoding('utf8')

#
if 'SLACK_TOKEN' in os.environ:
    SLACK_TOKEN=os.environ['SLACK_TOKEN']
sc = slack(SLACK_TOKEN)


#
NAMESPACES=[]
if 'NAMESPACES' in os.environ:
    NAMESPACES=os.environ['NAMESPACES'].split()
LOCAL_KUBECONFIG=False
if 'LOCAL_KUBECONFIG' in os.environ:
    LOCAL_KUBECONFIG=True
bot = k8s_bot(sc,NAMESPACES,LOCAL_KUBECONFIG)


def main():
    if sc.rtm_connect(with_team_state=False):
        print "Connected."
        while sc.server.connected is True:
            for message in sc.rtm_read():
                if message.get("type") == "message" and bot.isForMe(message,sc.botId):
                    print message
                    # print message.get("text")
                    messageSplited=message.get("text").strip().split()
                    print messageSplited
                    if len(messageSplited) > 1:
                        cmd = bot.checkCommand(messageSplited[1])
                        if cmd:
                            execCommand = getattr(bot,cmd,None)
                            if callable(execCommand):
                                execCommand(message)
                            else:
                                print "Command '%s' not found." % cmd
                        else:
                            bot.generalHelp(message,True)
                    else:
                        bot.generalHelp(message,True)
            time.sleep(1)


if __name__ == '__main__':
    main()