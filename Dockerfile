FROM alpine

COPY src /

RUN apk update \
    && apk add py-pip \
    && pip install -r requirements.txt \
    && rm -rf /var/cache/apk/* \
    && rm -rf /root/.cache/pip

ENTRYPOINT ["python","-u","run-bot.py"]
